# recipe-app-api-proxy

Recipe app API Proxy

## Usage

### Environement Variables:

* `Listen Port` - Port to listen on (default: `8080`)
* `APP_HOST` - Hostname of the app to forward to requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
 